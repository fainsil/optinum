"""
Ce fichier contient toutes fonctions utilisés dans les tests des algorithmes :
        - L'algorithme de Newton
        - Les régions de confiance
        - Le Lagrangien augmenté

"""
# Les points initiaux
## pour les problèmes sans contraintes
struct Pts_sans_contraintes
    x001
    x002
    x011
    x012
    x021
    x022
    x023
end
pts1 = Pts_sans_contraintes(
    -pi / 2 + 0.5,
    pi / 2,
    [1; 0; 0],
    [10; 3; -2.2],
    [-1.2; 1],
    [10; 0],
    [0; 1 / 200 + 1 / 10^12]
)

## pour les problèmes avec contraintes
struct Pts_avec_contraintes
    x01
    x02
    x03
    x04
end
pts2 = Pts_avec_contraintes(
    [0; 1; 1],
    [0.5; 1.25; 1],
    [1; 0],
    [sqrt(3) / 2; sqrt(3) / 2]
)

# Fonctions de test

"""
La zéroième fonction de test

# Expression
    fct0(x) = sin(x)
"""
fct0(x) = sin.(x)
# la gradient de la fonction fct0
grad_fct0(x) = cos.(x)
# la hessienne de la fonction fct0
hess_fct0(x) = -sin.(x)
# solutions de la fonction fct0
sol_exacte_fct0 = -pi / 2

""" 
La première fonction de test

# Expression
    fct1(x) = 2 * (x[1] + x[2] + x[3] - 3)^2 + (x[1] - x[2])^2 + (x[2] - x[3])^2
"""
fct1(x) = 2 * (x[1] + x[2] + x[3] - 3)^2 + (x[1] - x[2])^2 + (x[2] - x[3])^2
# la gradient de la fonction fct1
grad_fct1(x) = [
    4 * (x[1] + x[2] + x[3] - 3) + 2 * (x[1] - x[2])
    4 * (x[1] + x[2] + x[3] - 3) - 2 * (x[1] - x[2]) + 2 * (x[2] - x[3])
    4 * (x[1] + x[2] + x[3] - 3) - 2 * (x[2] - x[3])
]
# la hessienne de la fonction fct1
hess_fct1(x) = [
    6 2 4
    2 8 2
    4 2 6
]
# solutions de la fonction fct1
sol_exacte_fct1 = [1; 1; 1] # = -hess_fct1(x011) \ grad_fct1(zero(similar(x011)))
sol_fct1_augm = [0.5; 1.25; 0.5] # pour les problèmes avec contraintes


"""
La deuxième fonction de test

# Expression
    fct2(x) = 100 * (x[2] - x[1]^2)^2 + (1 - x[1])^2
"""
fct2(x) = 100 * (x[2] - x[1]^2)^2 + (1 - x[1])^2
# la gradient de la fonction fct2
grad_fct2(x) = [
    -400 * x[1] * (x[2] - x[1]^2) - 2 * (1 - x[1])
    200 * (x[2] - x[1]^2)
]
#la hessienne de la fonction fct2
hess_fct2(x) = [
    -400*(x[2]-3*x[1]^2)+2 -400*x[1]
    -400*x[1] 200
]
# solutions de la fonction fct2 
sol_exacte_fct2 = [1; 1]
sol_fct2_augm = [0.9072339605110892; 0.82275545631455] # pour les problèmes avec contraintes

"""
La première contrainte

# Expression
    contrainte1(x) =  x[1]+x[3]-1
"""
contrainte1(x) = x[1] + x[3] - 1
grad_contrainte1(x) = [1; 0; 1]
hess_contrainte1(x) = [0 0 0; 0 0 0; 0 0 0]

"""
La deuxième contrainte

# Expression
    contrainte2(x) =  (x[1]^2) + (x[2]^2) -1.5
"""
contrainte2(x) = (x[1]^2) + (x[2]^2) - 1.5
grad_contrainte2(x) = [2 * x[1]; 2 * x[2]]
hess_contrainte2(x) = [2 0; 0 2]

# Affichage les sorties de l'algorithme des Régions de confiance
function afficher_resultats(algo, nom_fct, point_init, xmin, fxmin, flag, sol_exacte, nbiters)
    println("-"^80)
    printstyled("Résultats de : " * algo * " appliqué à " * nom_fct * " au point initial ", point_init, " :\n", bold = true, color = :blue)
    println("  * xsol       = ", xmin)
    println("  * f(xsol)    = ", fxmin)
    println("  * nb_iters   = ", nbiters)
    println("  * flag       = ", flag)
    println("  * sol_exacte = ", sol_exacte)
end

function afficher_resultats_gct(algo, sol, sol_exacte)
    println("-"^80)
    printstyled("Résultats de : " * algo, " :\n", bold = true, color = :blue)
    println("  * sol        = ", sol)
    println("  * sol_exacte = ", sol_exacte)
end

function afficher_resultats_cauchy(algo, flag, sol, sol_exacte)
    afficher_resultats_gct(algo, sol, sol_exacte)
    println("  * flag       = ", flag)
end
@doc doc"""
Résolution des problèmes de minimisation sous cs d'égalités

# Syntaxe
```julia
Lagrangien_Augmente(algo,f,c,gradf,hessf,grad_c,
			hess_c,x_0,options)
```

# Entrées
  * **algo** 		   : (String) l'algorithme sans cs à utiliser:
    - **"newton"**  : pour l'algorithme de Newton
    - **"cauchy"**  : pour le pas de Cauchy
    - **"gct"**     : pour le gradient conjugué tronqué
  * **f** 		   : (Function) la ftion à minimiser
  * **c**	   : (Function) la c [x est dans le domaine des cs ssi ``c(x)=0``]
  * **gradf**       : (Function) le gradient de la ftion
  * **hessf** 	   : (Function) la hessienne de la ftion
  * **grad_c** : (Function) le gradient de la c
  * **hess_c** : (Function) la hessienne de la c
  * **x_0** 			   : (Array{Float,1}) la première composante du point de départ du Lagrangien
  * **options**		   : (Array{Float,1})
    1. **epsilon** 	   : utilisé dans les critères d'arrêt
    2. **tol**         : la tolérance utilisée dans les critères d'arrêt
    3. **itermax** 	   : nombre maximal d'itération dans la boucle principale
    4. **lambda_0**	   : la deuxième composante du point de départ du Lagrangien
    5. **mu_0,tau** 	   : valeurs initiales des variables de l'algorithme

# Sorties
* **xmin**		   : (Array{Float,1}) une approximation de la solution du problème avec cs
* **fxmin** 	   : (Float) ``f(x_{min})``
* **flag**		   : (Integer) indicateur du déroulement de l'algorithme
   - **0**    : convergence
   - **1**    : nombre maximal d'itération atteint
   - **(-1)** : une erreur s'est produite
* **niters** 	   : (Integer) nombre d'itérations réalisées

# Exemple d'appel
```julia
using LinearAlgebra
f(x)=100*(x[2]-x[1]^2)^2+(1-x[1])^2
gradf(x)=[-400*x[1]*(x[2]-x[1]^2)-2*(1-x[1]) ; 200*(x[2]-x[1]^2)]
hessf(x)=[-400*(x[2]-3*x[1]^2)+2  -400*x[1];-400*x[1]  200]
algo = "gct" # ou newton|gct
x_0 = [1; 0]
options = []
c(x) =  (x[1]^2) + (x[2]^2) -1.5
grad_c(x) = [2*x[1] ;2*x[2]]
hess_c(x) = [2 0;0 2]
output = Lagrangien_Augmente(algo,f,c,gradf,hessf,grad_c,hess_c,x_0,options)
```
"""
function Lagrangien_Augmente(algo, f::Function, c::Function, gradf::Function, hessf::Function, grad_c::Function, hess_c::Function, x_0, options)

  if options == []
    epsilon = 1e-8
    tol = 1e-5
    itermax = 1000
    lambda_0 = 2
    mu_0 = 100
    tau = 2
  else
    epsilon = options[1]
    tol = options[2]
    itermax = options[3]
    lambda_0 = options[4]
    mu_0 = options[5]
    tau = options[6]
  end

  _L(x, lambda, mu) = f(x) + lambda' * c(x) + mu / 2 * norm(c(x))^2
  _gradL(x, lambda, mu) = gradf(x) + lambda' * grad_c(x) + mu * c(x) * grad_c(x)
  _hessL(x, lambda, mu) = hessf(x) + lambda' * hess_c(x) + mu * (c(x) * hess_c(x) + grad_c(x) * grad_c(x)')

  flag = -1

  eta_chap_0 = 0.1258925
  alpha = 0.1
  beta = 0.9
  epsilon_0 = 1 / mu_0
  eta_0 = eta_chap_0 / (mu_0^alpha)

  k = 0
  x_k = x_0
  x_k1 = x_0
  lambda_k = lambda_0
  mu_k = mu_0
  eta_k = eta_0
  epsilon_k = epsilon_0

  while true

    L(x) = _L(x, lambda_k, mu_k)
    gradL(x) = _gradL(x, lambda_k, mu_k)
    hessL(x) = _hessL(x, lambda_k, mu_k)

    # a
    if algo == "newton"
      x_k1, _, _, _ = Algorithme_De_Newton(L, gradL, hessL, x_k, [itermax epsilon tol])
    elseif algo == "cauchy" || algo == "gct"
      x_k1, _, _, _ = Regions_De_Confiance(algo, L, gradL, hessL, x_k, [10.0 0.5 2 0.25 0.75 2.0 itermax epsilon tol])
    end

    if norm(c(x_k1)) <= eta_k # b
      lambda_k1 = lambda_k + mu_k * c(x_k1)
      mu_k1 = mu_k
      epsilon_k1 = epsilon_k / mu_k
      eta_k1 = eta_k / (mu_k^beta)
    else # c
      lambda_k1 = lambda_k
      mu_k1 = tau * mu_k
      epsilon_k1 = epsilon_0 / mu_k1
      eta_k1 = eta_chap_0 / (mu_k1^alpha)
    end


    if norm(_gradL(x_k, lambda_k, 0)) <= max(tol * norm(_gradL(x_0, lambda_0, 0)), epsilon) && norm(c(x_k)) <= max(tol * norm(c(x_0), epsilon))
      flag = 0
      break
    elseif k >= itermax
      flag = 1
      break
    end

    k += 1
    x_k = x_k1
    lambda_k = lambda_k1
    mu_k = mu_k1

  end

  # println("lambda_k=", lambda_k)
  # println("mu_k=", mu_k)

  xmin = x_k1
  f_min = f(xmin)
  return xmin, f_min, flag, k
end

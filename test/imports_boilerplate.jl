using Pkg
Pkg.status()
Pkg.add("DifferentialEquations")
Pkg.add("LinearAlgebra");
Pkg.add("Markdown")
using LinearAlgebra
using Markdown
using Test
# using TestOptinum
# using Documenter

include("../src/Algorithme_De_Newton.jl")
include("../src/Gradient_Conjugue_Tronque.jl")
include("../src/Lagrangien_Augmente.jl")
include("../src/Pas_De_Cauchy.jl")#
include("../src/Regions_De_Confiance.jl")

#include("cacher_stacktrace.jl")
#cacher_stacktrace()

# Tolérance pour les tests d'égalité
tol_erreur = sqrt(eps())

# ajouter les fonctions de test
include("fonctions_de_tests.jl")
include("tester_algo_newton.jl")
include("tester_pas_de_cauchy.jl")
include("tester_gct.jl")
include("tester_regions_de_confiance.jl")
include("tester_lagrangien_augmente.jl")
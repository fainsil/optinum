@doc doc"""
Minimise le problème : ``min_{||s||< \delta_{k}} q_k(s) = s^{t}g + (1/2)s^{t}Hs``
                        pour la ``k^{ème}`` itération de l'algorithme des régions de confiance

# Syntaxe
```julia
sk = Gradient_Conjugue_Tronque(fk,gradfk,hessfk,option)
```

# Entrées :   
   * **gradfk**           : (Array{Float,1}) le gradient de la fonction f appliqué au point xk
   * **hessfk**           : (Array{Float,2}) la Hessienne de la fonction f appliqué au point xk
   * **options**          : (Array{Float,1})
      - **delta**    : le rayon de la région de confiance
      - **max_iter** : le nombre maximal d'iterations
      - **tol**      : la tolérance pour la condition d'arrêt sur le gradient


# Sorties:
   * **s** : (Array{Float,1}) le pas s qui approche la solution du problème : ``min_{||s||< \delta_{k}} q(s)``

# Exemple d'appel:
```julia
gradf(x)=[-400*x[1]*(x[2]-x[1]^2)-2*(1-x[1]) ; 200*(x[2]-x[1]^2)]
hessf(x)=[-400*(x[2]-3*x[1]^2)+2  -400*x[1];-400*x[1]  200]
xk = [1; 0]
options = []
s = Gradient_Conjugue_Tronque(gradf(xk),hessf(xk),options)
```
"""
function Gradient_Conjugue_Tronque(gradfk, hessfk, options)

    "# Si option est vide on initialise les 3 paramètres par défaut"
    if options == []
        delta_k = 2
        max_iter = 100
        tol = 1e-6
    else
        delta_k = options[1]
        max_iter = options[2]
        tol = options[3]
    end

    n = length(gradfk)

    g_0 = gradfk
    g_j = g_0
    g_j1 = -1

    p_j = -g_0
    p_j1 = -1

    s_j = zeros(n)
    s_j1 = -1

    kappa_j = -1

    sigma_j = -1

    alpha_j = -1

    beta_j = -1

    s = -1

    q(s) = gradfk' * s + s' * hessfk * s / 2

    if norm(gradfk) == 0
        s = zeros(size(gradfk))
    else
        for j = 0:max_iter

            # a
            kappa_j = p_j' * hessfk * p_j

            # b
            if kappa_j <= 0
                a = sum(p_j .^ 2)
                b = 2 * sum(p_j .* s_j)
                c = sum(s_j .^ 2) - delta_k^2

                discriminant = b^2 - 4 * a * c
                x1 = (-b + sqrt(discriminant)) / (2 * a)
                x2 = (-b - sqrt(discriminant)) / (2 * a)

                v1 = q(s_j + x1 * p_j)
                v2 = q(s_j + x2 * p_j)

                if v1 < v2
                    sigma_j = x1
                else
                    sigma_j = x2
                end

                s = s_j + sigma_j * p_j
                break
            end

            # c
            alpha_j = g_j' * g_j / kappa_j

            # d
            if norm(s_j + alpha_j * p_j) >= delta_k
                a = sum(p_j .^ 2)
                b = 2 * sum(p_j .* s_j)
                c = sum(s_j .^ 2) - delta_k^2

                discriminant = b^2 - 4 * a * c
                x1 = (-b + sqrt(discriminant)) / (2 * a)
                x2 = (-b - sqrt(discriminant)) / (2 * a)

                if x1 > 0
                    sigma_j = x1
                else
                    sigma_j = x2
                end

                s = s_j + sigma_j * p_j
                break
            end

            # e 
            s_j1 = s_j + alpha_j * p_j

            # f
            g_j1 = g_j + alpha_j * hessfk * p_j

            # g
            beta_j = (g_j1' * g_j1) / (g_j' * g_j)

            # h
            p_j1 = -g_j1 + beta_j * p_j

            # i
            if norm(g_j1) <= tol * norm(g_0)
                s = s_j1
                break
            end

            s_j = s_j1
            g_j = g_j1
            p_j = p_j1

        end
    end

    return s
end
